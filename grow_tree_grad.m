%
%   function [vss] = grow_tree_grad(img,grad)
%
%       This code compares an image to a reference image
%
%       INPUT:
%       img        = matrix of likelihood of a vessel (values normalized in
%                    [0,1])
%       grad       = gradient of the image (values normalized in [0,1])
%       mask       = binary mask that is zero on the dark outer region of
%                    the image
%
%       OUTPUTS:
%       vss        = binary map of the vessel tree
%

function [vss] = grow_tree_grad(img,grad,mask)

[h,w] = size(img);

% definition of the means and standard deviations for the classes
grad_col = grad(mask==1);
img_col = img(mask==1);

thr = graythresh(img_col);

mu_g = mean(grad_col);
sgm_g = std(grad_col);
mu_v = mean(img_col(img_col > thr));
sgm_v = std(img_col(img_col > thr));
mu_b = mean(img_col(img_col <= thr));
sgm_b = std(img_col(img_col <= thr));

% seed points for the background and the vessels
bgd = img <= mu_b;
vss = img >= mu_v;

% region growing algorithm of the background and the vessels
% we loop until we do reach a fixed point
a = 1;
N_old = 0;
N = sum(vss(:)) + sum(bgd(:));
while N_old < N
    bgd_cand = (img <= mu_b + a*sgm_b) & (grad <= mu_g) & (vss == 0);
    bgd = bgd | imreconstruct(bgd,bgd_cand);
    vss_cand = (img >= mu_v - a*sgm_v) & (grad <= mu_g + a*sgm_g) & (bgd == 0);
    vss = vss | imreconstruct(vss,vss_cand);
    a = a + 0.5;
    N_old = N;
    N = sum(vss(:)) + sum(bgd(:));
end

% end of the algorithm : we do not take into account the gradient
% anymore, and we loop until all the pixels were classified
a = 1;
while sum(vss(:)) + sum(bgd(:)) < h*w
    bgd_cand = (img <= mu_b + a*sgm_b) & (img >= mu_b - a*sgm_b) & (vss == 0);
    bgd = imreconstruct(bgd,bgd_cand);
    vss_cand = (img <= mu_v + a*sgm_v) & (img >= mu_v - a*sgm_v) & (bgd == 0);
    vss = imreconstruct(vss,vss_cand);
    a = a + 0.5;
end

end