%
%   function [tp, fn, fp] = comp(gen,ref)
%
%       This code compares an image to a reference image
%
%       INPUT:
%       gen        = image generated that we want to evaluate (values
%                    normalized in [0,1])
%       ref        = reference image (values normalized in [0,1])
%       mask       = mask (binary)
%
%       OUTPUTS:
%       tp         = number of true positives
%       tn         = number of true negatives
%       fp         = number of false positives
%       fn         = number of false negatives
%

function [tp, tn, fn, fp] = comp2(gen,ref,mask,i)
    ref_msk = ref.*mask;
    [h,w] = size(gen);
    img = zeros(h,w,3);
    img(:,:,1) = gen.*(1-ref_msk);
    img(:,:,2) = gen.*ref_msk;
    img2 = zeros(h,w);
    img2(gen==0) = 1;
    img(:,:,3) = img2.*ref_msk;
    global supervised_method
    filename = strcat(supervised_method,"/",supervised_method,"_result_",num2str(i),".png");
    imwrite(img,char(filename));

    tp = sum(sum(img(:,:,2)));
    fn = sum(sum(img(:,:,1)));
    fp = sum(sum(img(:,:,3)));
    tn = sum(mask(:)) - tp - fn - fp;

end