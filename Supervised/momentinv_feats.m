function [X, IDX] = momentinv_feats(imdata, I, J, w)
%\ Moment invariant-based features
%\ Feature Extraction step from Marin et. al. 

    [M,N] = size(imdata);   
    X     = NaN*ones(2,length(I));
    IDX   = zeros(1,length(I));
    
    %/ 
    n       = 17;
    sigma   = 1.7;
    G17     = fspecial('gaussian', [n n], sigma);
    
    for idx = 1:length(I)

        i = I(idx);
        j = J(idx);
        dw = floor(w/2);
        
        IMij = imdata(i,j);
        
        il = max(1, i - dw);        iu = min(M, i + dw);
        jl = max(1, j - dw);        ju = min(N, j + dw);
            
        Sw   = imdata(il:iu, jl:ju);
        
        if max(Sw(:)) > (10^-12) 
            IDX(idx) = 1;
        end
        
        %/ handles edge cases %/
        [mS,nS] = size(Sw);
        G = G17;
        if mS < w
            if il == 1
                G = G(1+(w-mS):end,:);
            else
                G = G(1:mS,:);
            end 
        end
        if nS < w
            if jl == 1
                G = G(:,1+(w-nS):end);
            else
                G = G(:,1:nS);
            end   
        end
        
        IMhu = Sw.*G;
        
        [jj, ii] = meshgrid(jl:ju, il:iu);
        
        phi1 = eta_pq(IMhu, 2, 0, ii, jj) + eta_pq(IMhu, 0, 2, ii, jj);
        phi2 = (eta_pq(IMhu, 2, 0, ii, jj) + eta_pq(IMhu, 0, 2, ii, jj))^2 ...
                 + 4*(eta_pq(IMhu, 1, 1, ii, jj))^2;
                
       if (log(abs(phi1)) < -10^12) || (log(abs(phi2)) < -10^12)
           IDX(idx) = 0;
       end
             
        X(:,idx) = [ log(abs(phi1))  ;...
                     log(abs(phi2))  ]; 
    end
    
end
    
    function M = m_pq(IM, p, q, ii, jj)      
        M = sum(sum((ii.^p).*(jj.^q).*IM));      
    end
    
    function U = mu_pq(IM, p, q, ii, jj)
        m00  = m_pq(IM, 0, 0, ii, jj);
        m10  = m_pq(IM, 1, 0, ii, jj);
        m01  = m_pq(IM, 0, 1, ii, jj);
    
        ibar = m10/m00;
        jbar = m01/m00;
        
        U    = sum(sum(((ii-ibar).^p).*((jj-jbar).^q).*IM));      
    end
    
    function E = eta_pq(IM, p, q, ii, jj)
        uqp   = mu_pq(IM, p, q, ii, jj);
        u00   = mu_pq(IM, 0, 0, ii, jj);
        
        gamma = ((p+q)/2) + 1; 
        
        E     = uqp/(u00^gamma);
    end
    
 