function imdata2 = vclr_removal(imdata, D, N)
%\ Vessel Central Light Reflex Removal
%\ Removes light streaks in the middle of vessels (light reflex)
%\ Preprocessing step from Marin et. al. 
%\ morphological opening using 3-pixel diameter disk structuring element (with N = 8) 

    SE      = strel('disk', floor(D/2), N);
    imdata2 = imopen(imdata,SE);

end