function imdata2 = bkgd_homogen_noblur(imdata, immask)
%\ Background Homogenization
%\ Corrects intensity variation in background due to non-uniform illumination
%\ Preprocessing step from Marin et. al.

    %/ Noise reduction using 3x3 mean filter:   
    n       = 3;
    F1      = ones(n)/(n^2);
    imdata2 = conv2(imdata,F1,'same');
    
    %/ Noise smoothing with 9x9 Gaussian kernel:
    n       = 9;
    sigma   = 1.8;
    F2      = fspecial('gaussian', [n n], sigma);
    imdata2 = imfilter(imdata2, F2, 'replicate', 'conv');
    
    %/ Background image produced with 69x69 mean filter:
    n     = 69;
    imbkg = masked_meanfilt(imdata2, immask, n);
    
    %/ Compute difference of images (background subtraction)
    D = imdata - imbkg;
    
    Dmin = min(D(immask==1));
    Dmax = max(D(immask==1));
    
    imdata2 = floor(256*((1/(Dmax-Dmin))*(D - Dmin)));
       
    g_max = mode(imdata2(immask==1));
    
    imdata2 = imdata2 + (128 - g_max);
    imdata2(imdata2<0)   = 0;
    imdata2(imdata2>255) = 255;    
    imdata2(immask==0)   = 0;
    
    imdata2 = imdata2/255;
    
end