function imdata2 = vessel_enhance(imdata, r)
%\ Vessel Enhancement
%\ generates vessel-enhanced image using morphological top-hat operation
%\ Preprocessing step from Marin et. al. 

    SE      = strel('disk', r, 8);
    imtemp  = imcomplement(imdata); 
    imdata2 = imtemp - imopen(imtemp,SE);

end