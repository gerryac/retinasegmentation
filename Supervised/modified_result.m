load(strcat(supervised_method,'/','tested_model.mat'))

%/ plot, computes statistics
%[m, n] = size(TEST_NN{1});
clear imdata;
for i= 1:20
    %figure();
        imdata{i} = double(TEST_NN{i} > 0.5);
        %imdata{i} =  postprocess_result(imdata{i}, MASK_data{i});
        
        %imagesc(imdata{i} - 2*GT_data1{i});
        %axis square; 
        %colormap([0 0 1; 0 1 0; 0 0 0; 1 0 0]);   
        
    [tp(i), tn(i), fn(i), fp(i)] = comp2(imdata{i} ,GT_data1{i},MASK_data{i},i)  ;   
     
end


accuracy    = ((tp + tn)./(tp + tn + fn + fp))';
disp(['accuracy: ', num2str(mean(accuracy))])

precision  = (tp./(tp + fp))';
disp(['precision: ', num2str(mean(precision))])

sensitivity = (tp ./ (tp + fn))';
disp(['sensitivity: ', num2str(mean(sensitivity))])

specificity = (tn ./ (tn + fp))';
disp(['specificity: ', num2str(mean(specificity))])

performance.accuracy    = accuracy;
performance.precision   = precision;
performance.sensitivity = sensitivity;
performance.specificity = specificity;

im_result = imdata;
save(strcat(supervised_method,'/',supervised_method,'_results.mat'), 'im_result')
save(strcat(supervised_method,'/',supervised_method,'_performance.mat'), 'performance')
