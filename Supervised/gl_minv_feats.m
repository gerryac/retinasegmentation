function [X, IDX] = gl_minv_feats(imdata, I, J, gl_w, minv_w)
%\ Gray-level-based features and Moment invariant-based features
%\ Feature Extraction step from Marin et. al. 

    X = NaN*ones(7, length(I));

    X(1:5,:)       = graylevel_feats(imdata, I, J, gl_w);
   [X(6:7,:), IDX] = momentinv_feats(imdata, I, J, minv_w);

end