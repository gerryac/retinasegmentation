function X = graylevel_feats(imdata, I, J, w)
%\ Gray-level-based features
%\ Feature Extraction step from Marin et. al. 

    [M,N] = size(imdata);   
    X = NaN*ones(5,length(I));
    
    for idx = 1:length(I)

        i = I(idx);
        j = J(idx);
        dw = floor(w/2);
        
        IMij = imdata(i,j);
        
        il = max(1, i - dw);        iu = min(M, i + dw);
        jl = max(1, j - dw);        ju = min(N, j + dw);
    
        Sw = imdata(il:iu, jl:ju);
        
        minSw  = min(Sw(:));
        maxSw  = max(Sw(:));
        meanSw = mean(Sw(:));
        stdSw  = std(Sw(:));
        
        X(:,idx)  = [  IMij - minSw  ;...
                      maxSw - IMij   ;...
                       IMij - meanSw ;...
                           stdSw     ;...
                           IMij      ];
                        
    end

end
