% NUS ISS 2018 - Team Palantir - CA 3%


global supervised_method;
supervised_method = 'gan'; %MODE = baseline(nn),svm,rf,gan

disp(strcat("Running ", supervised_method, " mode..."))


if strcmpi(supervised_method, 'gan')
    for i = 1:20
        if i < 10
            numstr = ['0',num2str(i)];
        else
            numstr = num2str(i);
        end
        
        TEST_NN{i} = im2double(imread(['gan/inference_outputs/DRIVE/',numstr,'_test.tif']));
        MASK_data{i} = im2double(imread([numstr,'_test_mask.gif']));
        GT_data1{i}  = im2double(imread([numstr,'_manual1.gif']));
        save(strcat(supervised_method,'/','tested_model'),'MASK_data','GT_data1','TEST_NN')
        
    end
else
    modelPath = strcat(supervised_method,"/","trained_model.mat");
    if exist(modelPath) == 2
        disp("Reuse pre-trained model");
    else
        modified_train;
    end

    testPath = strcat(supervised_method,"/","tested_model.mat");
    
    if exist(testPath) == 2
        disp("Reuse tested model");
    else
        modified_test;
    end 
    
end

modified_result;