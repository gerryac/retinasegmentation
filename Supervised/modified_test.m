 %% Extract features from test image %%
 
  %add_image_paths
 
 clear IMAGE_data GT_data MASK_data FEATS TARGET
 load(strcat(supervised_method,"/",'trained_model'),'model')
 
     %% parameters %%
        gl_w   = 9;
        minv_w = 17;
 
 test_list = 1:20;
 
 for k = 1:length(test_list)
 
     IMnum = test_list(k);
     disp(strcat("Testing on image ", num2str(IMnum), "..."));

     if IMnum < 10
        numstr = ['0',num2str(IMnum)];
     else
        numstr = num2str(IMnum);
     end

     rgb_image  = imread([numstr,'_test.tif']);
     mask_image = im2double(imread([numstr,'_test_mask.gif']));
     gt1_image  = im2double(imread([numstr,'_manual1.gif']));
     gt2_image  = im2double(imread([numstr,'_manual2.gif']));
     
    %imdata = preprocess_image2(rgb_image, mask_image, 1, IMnum);
     imdata = preprocess_image(rgb_image, mask_image, 1, IMnum);
     
     IMAGE_data{k} = imdata;
     MASK_data{k}  = mask_image;
     GT_data1{k}   = gt1_image;
     GT_data2{k}   = gt2_image;

     [I,J]         = find(mask_image==1); 
     [X_test, IDX] = gl_minv_feats(imdata, I, J, gl_w, minv_w);
     
     TEST = 0;
     
     %/ removes pixels with all zero neighborhood (these were causing NaNs)
     %/ (all-zero neighborhood pixels automatically classified as background)
     X_test = X_test(:,IDX==1);
     I      = I(IDX==1,1);
     J      = J(IDX==1,1);
     
        mu_i    = nanmean(X_test, 2);
        sigma_i = nanstd(X_test, [], 2);

        for i = 1:7   
            X_test(i,:) = (X_test(i,:) - mu_i(i))/sigma_i(i);
        end     
        
%% Test Network %%

     if supervised_method == "baseline"
         CLASS = sim(model, X_test);
     end
     
     if supervised_method == "svm" 
         CLASS = predict(model, transpose(X_test));
     end
     
     if supervised_method == "rf"
         CLASS = predict(model, transpose(X_test));
     end

     RESULT = zeros(size(imdata));
     for t = 1:length(I)    
        RESULT(I(t),J(t)) = CLASS(t);   
     end
     level = graythresh(RESULT(mask_image==1));

     %figure(); 
        %imagesc(double(RESULT >=level) - 2*gt1_image); 
            %axis square
            %colormap([0 0 1; 0 1 0; 0 0 0; 1 0 0])
            
     TEST_NN{k} = RESULT;   
     disp("Finished testing image ", num2str(IMnum));

 end
 
  save(strcat(supervised_method,'/','tested_model'), 'IMAGE_data', 'MASK_data', ...
       'GT_data1', 'GT_data2', 'model', 'TEST_NN')

