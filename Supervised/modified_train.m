%% Load and Preprocess training set %%

    %add_image_paths
    
    %% parameters %%
        gl_w   = 9;
        minv_w = 17;

        IMnum_list = [ 21 22 25 30 31];

        for k = 1:length(IMnum_list)

            IMnum = IMnum_list(k);

            rgb_image  = imread([num2str(IMnum),'_training.tif']);
            mask_image = im2double(imread([num2str(IMnum),'_training_mask.gif']));
            gt_image   = im2double(imread([num2str(IMnum),'_manual1.gif']));

           %imdata = preprocess_image(rgb_image, mask_image, 1, IMnum);
            imdata = preprocess_image2(rgb_image, mask_image, 1, IMnum);
            
            IMAGE_data{IMnum-20} = imdata;
            GT_data{IMnum-20}    = gt_image;
            MASK_data{IMnum-20}  = mask_image;

        end
        
        save(strcat(supervised_method,'/','preprocessed_images'), 'IMAGE_data', 'MASK_data', 'GT_data')

%% Select training pixels, extract features %%

    tr_image_list  = [ 21 22 25 30 31];
    tr_image_shift = [ 34  36  37  40  34 ;...
                       12  17  14  13  12];

     X_bkgd = [] ;
     X_ves  = [] ;

    for k = 1:length(tr_image_list)

        imdata = IMAGE_data{tr_image_list(k)-20};
        mask   = MASK_data{tr_image_list(k)-20};

        bkgd_image = zeros(size(imdata));
        ves_image  = zeros(size(imdata));

        bkgd_temp = double(imread([num2str(tr_image_list(k)),'_backgr.tif']))/255;
        try
            ves_temp  = double(rgb2gray(imread([num2str(tr_image_list(k)),'_caps.tif'])))/255;
        catch
            ves_temp  = double(imread([num2str(tr_image_list(k)),'_caps.tif']))/255;
        end

        bkgd_image((1+tr_image_shift(1,k)):(size(bkgd_temp,1)+tr_image_shift(1,k)), ...
                   (1+tr_image_shift(2,k)):(size(bkgd_temp,2)+tr_image_shift(2,k))) = bkgd_temp;
        ves_image( (1+tr_image_shift(1,k)):(size(ves_temp,1)+tr_image_shift(1,k)), ...
                   (1+tr_image_shift(2,k)):(size(ves_temp,2)+tr_image_shift(2,k)) )  = ves_temp;   

        [I_bkgd, J_bkgd] = find(bkgd_image==1 & imdata<0.1);
        [I_ves, J_ves]   = find(ves_image==1 & imdata>0.05);

        [temp_bkgd, IDX_bkgd] = gl_minv_feats(imdata, I_bkgd, J_bkgd, gl_w, minv_w);
        [temp_ves,  IDX_ves]  = gl_minv_feats(imdata, I_ves,  J_ves,  gl_w, minv_w);
        
        X_bkgd = [ X_bkgd, temp_bkgd(:,IDX_bkgd) ];
        X_ves  = [ X_ves,  temp_ves(:,IDX_ves) ];

    end

    mu_i    = nanmean([X_bkgd, X_ves], 2);
    sigma_i = nanstd([X_bkgd, X_ves], [], 2);

    for i = 1:7   
        X_bkgd(i,:) = (X_bkgd(i,:) - mu_i(i))/sigma_i(i);
        X_ves(i,:)  = (X_ves(i,:)  - mu_i(i))/sigma_i(i);  
    end

    FEATS  = [X_bkgd, X_ves];
    TARGET = [zeros(1,size(X_bkgd,2)), ones(1,size(X_ves,2))];

    if supervised_method == "baseline"
        n = 7;
        hlayers = [15 15 15];
        model = newff(FEATS, TARGET, hlayers, {'purelin', 'purelin', 'purelin','logsig'});
        model.trainParam.epochs = 50;
        model.trainParam.goal   = 0.01;
        model.divideParam.trainRatio = 0.7;
        model.divideParam.valRatio   = 0.3;    
        model = train(model,FEATS,TARGET); 
    end
    
    if supervised_method == "svm"
        model = fitrsvm(transpose(FEATS),transpose(TARGET),'Standardize',true,'KernelFunction','polynomial','KernelScale','auto');
    end
    
    if supervised_method == "rf"
        model = TreeBagger(50,transpose(FEATS),transpose(TARGET),'Method','regression');
    end
    
    save(strcat(supervised_method,'/','trained_model'), 'IMAGE_data', 'MASK_data', 'GT_data', 'FEATS', 'TARGET', 'model')
    
   