function imdata = preprocess_image(rgb_image, mask_image, show_fig, IMnum)
%\ Preprocessing step from Marin et. al. 

    if nargin == 2
        show_fig = 0;
        IMnum = [];
    end

    imdata     = im2double(rgb_image(:,:,2));
    
    if show_fig == 1
        %figure();
        %subplot(1,3,1); imshow(imdata); title(['Original image: ',num2str(IMnum),'\_training.tif'])
    end

    imdata = vclr_removal(imdata, 3, 8);
    imdata = bkgd_homogen(imdata, mask_image);
    
    if show_fig == 1
        %subplot(1,3,2); imshow(imdata); title('Background homogenization')
    end
    
    imdata = vessel_enhance(imdata, 8);
    imdata(mask_image==0) = 0;

    if show_fig == 1
        %subplot(1,3,3); imshow(imdata/max(imdata(:))); title('Vessel Enchancement')
    end