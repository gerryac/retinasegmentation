function thresh = Thresh_alg(gray_im)

[M,N] =size(gray_im);

% extend mtx dimentions
mod_mtx = zeros(256,256);
for m = 1:M-1
    for n = 1:N-1        
       mod_mtx(gray_im(m,n)+1,gray_im(m,n+1)+1) = mod_mtx(gray_im(m,n)+1,gray_im(m+1,n+1)+1) + 1;
    end
end

% normalize mtx
prob_mtx = mod_mtx/sum(mod_mtx(:));

max_entrop = -100;  
for i=1:255

    s_first_prob = prob_mtx(1:i,1:i);
    first_prob = sum(s_first_prob(:));
    s_last_prob = prob_mtx(i+1:end,i+1:end);
    last_prob = sum(s_last_prob(:));
    entrop = -0.5*(last_prob*log2(last_prob+0.0000001)+first_prob*log2(first_prob+0.0000001));

    if entrop >= max_entrop
        max_entrop = entrop;
        thresh = i;
    end
end