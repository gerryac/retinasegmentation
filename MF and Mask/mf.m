function seg_img = mf(green_ch,sigma)

[M,N] = size(green_ch);

% Optional parameters
T = 3*sigma;
x = -T:T;
L = 9;


% Gaussian kernel definitions
match_filt1 = exp(-(x.*x)/(2*sigma*sigma)); 
match_filt2 = max(match_filt1)-match_filt1; 
input_neighborhood1 = repmat(match_filt2,[L 1]); 
sum_input_neighborhood = sum(input_neighborhood1(:));
mean = sum_input_neighborhood/(length(x)*L);
input_neighborhood2 = input_neighborhood1 - mean;
input_neighborhood = input_neighborhood2/sum_input_neighborhood;


kernel{1} = zeros(15,16);
for i = 1:L
    for j = 1:length(x)
        kernel{1}(i+3,j+1) = input_neighborhood(i,j);
    end
end 

% kernel rotation for 11 additional directions
for k=1:11 
    ang = 15*k;
    kernel{k+1} = imrotate(kernel{1},ang,'bicubic','crop');
end 

% convolution with filters
for k=1:12
    dir_options{k} = conv2(green_ch, kernel{k}, 'same');
end

% finding the correct orientation which is the orientation with maximum filter response
seg_img = zeros(M,N); 
for i=1:M
    for j=1:N
       seg_img(i,j) = max( [dir_options{1}(i,j), dir_options{2}(i,j), dir_options{3}(i,j), dir_options{4}(i,j), dir_options{5}(i,j), dir_options{6}(i,j),... 
                            dir_options{7}(i,j), dir_options{8}(i,j), dir_options{9}(i,j), dir_options{10}(i,j), dir_options{11}(i,j), dir_options{12}(i,j)]);
        
    end
end


% normalization
min_val = abs(min(seg_img(:)));
for m = 1:M
    for n = 1:N
        seg_img(m,n) = seg_img(m,n) + min_val;
    end
end

max_val = max(max(seg_img));
for m = 1:M
    for n = 1:N
        seg_img(m,n) = round(seg_img(m,n)*255/max_val);
    end
end

