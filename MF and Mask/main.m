
% This is the main function to perform blood vessel extraction
% final_img is the final binary segmented image
% optical_disk_mask is the mask used for the modified hybrid algorithm
% prob is the probability map used for the original hybrid algorithm

array = ['01';'02';'03';'04';'05';'06';'07';'08';'09';'10';'11';'12';'13';'14';'15';'16';'17';'18';'19';'20'];
for idx = 1:20
    
    % Read images
    I= im2double(imread([num2str(array(idx,:)) '_test.tif'])); 
    G = zeros(size(I));
    im_gray = rgb2gray(I);
    [M,N] = size(im_gray);
    green_ch = I(:,:,2); 

    % creating the mask
    h = rgb2hsv(I);
    mask1 =  h(:,:,3) > 0.1;
    mask = imerode(mask1,strel('disk',10));


    % performing matched filtering 
    sigma = 2.0;
    im1 = mf(green_ch,sigma);
    
%% code to create probability map for original hybrid algorithm

    medval = median(median(im1));
    im1_prob = im1.*double(mask);
    for n = 1:size(im1,1)
        for m = 1:size(im1,2)
            if (im1_prob(n,m)<medval)
                im1_prob(n,m) = medval;
            end
        end
    end
    
    %normalized probability map used for original hybrid algorithm
    prob{idx} = (im1_prob-medval)/max(max(im1_prob));

    

%% Code to create mask for the modified Hybrid algorithm

    tmp = adapthisteq(green_ch.*mask);
    norm_tmp = tmp./max(max(tmp));
    initial_thresh = 180/255;
    optical_disk_mask{idx} = imdilate(tmp>initial_thresh,strel('disk',20));

    thresh = Thresh_alg(im1);
    im2 = im1.*mask; 

    for i=1:M
        for j=1:N
            if im1(i,j)>=thresh 
                im2(i,j)=0;
            end
        end
    end
    J = ~im2bw(im2); 



    % removing regions based on area and proximity
    
    [Label,Num] = bwlabel(J);
    labeled_img = zeros(Num+1,1);
    for i=1:M
        for j=1:N
            labeled_img(double(Label(i,j))+1) = labeled_img(double(Label(i,j))+1) + 1;
        end
    end
    
    sorted_labeled_img = sort(labeled_img);
    
    cp = 950;
    for i=1:M
        for j=1:N
            if (labeled_img(double(Label(i,j)+1)) > cp) && (labeled_img(double(Label(i,j)+1)) ~= sorted_labeled_img(Num+1,1))
                J(i,j) = 0;
            else
                J(i,j) = 1;
            end
        end
    end
    for i=1:M
        for j=1:N
            if mask(i,j)==0
                J(i,j)=1;
            end
        end
    end
    final_img = ~J;
    figure; imshow(final_img)
    
%% calculating performance measures and displaying the final comparison image 
   
    ref = im2double(imread([num2str(array(idx,:)) '_manual1.gif']));
    [tp, tn, fp, fn] = comp(final_img,ref,mask);
    accuracy(idx) = (tp+tn)/sum(sum(mask))*100;
    precision(idx) = tp/(tp+fp)*100;
    sensitivity(idx) = tp/(tp+fn)*100;
    specificity(idx) = tn/(tn+fp)*100;
    
end

