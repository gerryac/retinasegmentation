%
%   function [tp, fn, fp] = comp(gen,ref)
%
%       This code compares an image to a reference image
%
%       INPUT:
%       rgb        = original retinal image
%       mask       = mask (binary)
%       mask_s     = smaller mask (binary)
%
%       OUTPUTS:
%       result     = probability of vessel
%

function [result] = morph(rgb,mask,mask_s)

[h,w] = size(rgb(:,:,1));
ycbcr = rgb2ycbcr(rgb);

lum = double(ycbcr(:,:,1));
lum_norm = lum/max(lum(:));
lum_mask = lum_norm.*mask;

% top-hat operation
tophat_1 = zeros(h,w,8);
sc = strel('disk',2);
for siz = 1:8
    so = strel('disk',siz);
    A = zeros(h,w,2);
    A(:,:,1) = mask - lum_mask;
    A(:,:,2) = imopen(imclose(mask - lum_mask,sc),so);
    tophat_1(:,:,siz) = (mask - lum_mask - min(A,[],3)).*mask_s;
end
tophat = zeros(h,w,4);
for i = 1:4
    tophat(:,:,i) = (tophat_1(:,:,2*i-1) + tophat_1(:,:,2*i-1)) /2;
end

% take the max on the different scales
tophat_sum = zeros(h,w,4);
for i = 1:4
    tophat_sum(:,:,i) = tophat(:,:,i)/i^2;
end
tophat_sum = tophat_sum(:,:,2:4);
result = max(tophat_sum,[],3)/max(tophat_sum(:));
    
end