function imdata2 = masked_meanfilt(imdata, im_mask, n)
%\ performs mean filtering where pixels outside of specified region are
%\ replaced by mean of valid pixels within box

    imdata(im_mask==0) = NaN;
    [M,N] = size(imdata);
    
    F       = ones(n)/(n^2);
    imtemp  = conv2(imdata,F,'same');    
    imdata2 = imtemp;
    
    dn = floor(n/2);

    for i = 1:M
        for j = 1:N 
            if( isnan(imtemp(i,j)) && (im_mask(i,j)==1))              
                il = max(1,i - dn);     iu = min(M,i + dn);
                jl = max(1,j - dn);     ju = min(N,j + dn);                
                newval = nanmean(nanmean(imdata(il:iu, jl:ju)));
                imdata2(i,j) = newval;
            end
        end
    end
    
    %imdata2(im_mask==0) = 0;
    
end