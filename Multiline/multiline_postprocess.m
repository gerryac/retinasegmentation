function imdata = multiline_postprocess(imdata)
%/ removes small connected components

    CC = bwconncomp(imdata);
    
     for i=1:CC.NumObjects; 
         sizek(i) = numel(CC.PixelIdxList{i}); 
         
         if sizek(i) < 100
             imdata(CC.PixelIdxList{i}) = 0;
         end  
     end;

end