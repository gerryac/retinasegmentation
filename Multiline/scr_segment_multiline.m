%% Multi-scale Line Detection %%

    % scr_preprocess_nodenoise
    load('test_images_no_denoising.mat') %/ computed in <scr_preprocess_nodenoise>
    clear imdata

    for i = 1:20; 
        imdata{i} = IMAGE_data{i};
        immask{i} = MASK_data{i}; 

     %/ meant to reduce effect of bright circular region (optic disk)
        temp      = min( imdata{i}, conv2(double(imdata{i}>0.55), ones(20)/(20^2),'same'));
        image_avg = conv2(imdata{i}, ones(69)/(69^2),'same');
        imdata{i} = (imdata{i}.*(1-temp) + temp.*image_avg);

     %/ plot
        %figure(i); imshow([IMAGE_data{i} imdata{i}]);

    end;

    [M,N] = size(imdata{i});

    L = 3:2:15;

    for j = 1:length(L)

     %/ line detection element of size L(j)    
        w = 15;
        dw = floor(L(j)/2);
        template = zeros(w); 
        template(ceil(w/2),(ceil(w/2)-dw):(ceil(w/2)+dw)) = 1;

        F1      = ones(w)/(w^2);

     %/ rotates line in 10 degree increments   
        degrees = 0:10:180;
        for k = 1:length(degrees); 
            WL{k} = imrotate(template,degrees(k), 'bilinear','crop'); 
        end;

     %/ loops through images   
        for i = 1:20; 

            temp_line = zeros(size(imdata{i}));

         %/ inverts image
            temp = 1 - imdata{i};
            imageI{i} = temp;

         %/ computes maximum line response over multiple angles   
            temp_avg = conv2(temp,F1,'same');
            for k = 1:length(degrees)
                tempL = imfilter(temp, WL{k}/L(j));
                temp_line = max(temp_line, tempL);
            end

         %/ enlargest image mask (to eliminate edge effects)   
            tempmask = imerode(immask{i}, strel('disk', 10, 8)); 

         %/ normalization and scaling of response   
            RW = temp_line - temp_avg;
            idx = find(tempmask==1);
            Rprime{i}{j} = (RW - mean(RW(idx)))/std(RW(idx));
            Rprime{i}{j}(tempmask==0) = 0;

        end

    end

    for i = 1:20

        Rcomb{i} = zeros(M,N);
        for j = 1:length(L)
            Rcomb{i} = Rcomb{i} + L(j)*Rprime{i}{j};
        end
        Rcomb{i} = (Rcomb{i} + (imdata{i}))/(sum(L) + 1);

       %/ computes global threshold  
        level  = graythresh(Rcomb{i}(immask{i}==1));

        %figure(i);
        %imshow( [imageI{i}, 1-(Rcomb{i}>0.75*level); ...
        %        ~GT_data1{i}, multiline_postprocess(double(Rcomb{i}>0.75*level))]);


      %/ post-processing for binary result
        result{i} = multiline_postprocess(double(Rcomb{i}>0.75*level));

     %/ plot
        figure(i+20);
        imagesc(result{i} - 2*GT_data1{i} );
        axis square;
        colormap([0 0 1; 0 1 0; 0 0 0; 1 0 0])

     %/ computes performance statistics
        [tp(i), tn(i), fn(i), fp(i)] = comp(result{i},GT_data1{i},immask{i});

    end

    %/ performance statistics
        performance.accuracy    = ((tp + tn)./(tp + tn + fn + fp))';
        performance.precision   = (tp./(tp + fp))';
        performance.sensitivity = (tp ./ (tp + fn))';
        performance.specificity = (tn ./ (tn + fp))';

    %/ probability map version of results used by hybrid algorithm
         for i = 1:20; 
            tempim = Rcomb{i} - median(Rcomb{i}(:)); 
             im_result{i} = (1/max(tempim(:)))*tempim;
            im_result{i}(im_result{i} <0) = 0; 
         end
         save('multi-line_results', 'im_result')
         save('multi-line_performance', 'performance')
