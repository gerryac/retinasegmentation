%/ loads training images 

IMnum_list = 21:40;

        for k = 1:length(IMnum_list);

            IMnum = IMnum_list(k);

            rgb_image  = imread([num2str(IMnum),'_training.tif']);
            mask_image = im2double(imread([num2str(IMnum),'_training_mask.gif']));
            gt_image   = im2double(imread([num2str(IMnum),'_manual1.gif']));

         %/ uses only green channel   
            imdata     = im2double(rgb_image(:,:,2)); 
            
         %/ applies background homogenization (with no denoising)
            imdata = bkgd_homogen_noblur(imdata, mask_image);

            IMAGE_data{k} = imdata;
            GT_data{k}    = gt_image;
            MASK_data{k}  = mask_image;

        end

save('training_images_no_denoising', 'IMAGE_data', 'GT_data', 'MASK_data')


clear IMAGE_data GT_data MASK_data


test_list = 1:20;
 
 for k = 1:length(test_list)
 
     IMnum = test_list(k);

     if IMnum < 10
        numstr = ['0',num2str(IMnum)];
     else
        numstr = num2str(IMnum);
     end

     rgb_image  = imread([numstr,'_test.tif']);
     mask_image = im2double(imread([numstr,'_test_mask.gif']));
     gt1_image  = im2double(imread([numstr,'_manual1.gif']));
     gt2_image  = im2double(imread([numstr,'_manual2.gif']));
     
  %/ uses only green channel      
     imdata  = im2double(rgb_image(:,:,2));
     
  %/ applies background homogenization (with no denoising)  
     imdata  = bkgd_homogen_noblur(imdata, mask_image);
     
     IMAGE_data{k} = imdata;
     MASK_data{k}  = mask_image;
     GT_data1{k}   = gt1_image;
     GT_data2{k}   = gt2_image;
     
 end

 save('test_images_no_denoising', 'IMAGE_data', 'GT_data1', 'GT_data2', 'MASK_data')
