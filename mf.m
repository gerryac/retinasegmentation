function rt = mf(green_ch,sigma)
%{
tmp = adapthisteq(green_ch);
initial_thresh = 150;
thresh1 = green_ch<initial_thresh;
mask = im2double(imread('21_training_mask.gif'));
green_ch = tmp;
%}
[M,N] = size(green_ch);
T = 3*sigma;
x = -T:T;
L = 9;

match_filt1 = exp(-(x.*x)/(2*sigma*sigma)); 
match_filt2 = max(match_filt1)-match_filt1; 
input_neighborhood1 = repmat(match_filt2,[L 1]); 
sum_input_neighborhood = sum(input_neighborhood1(:));
mean = sum_input_neighborhood/(length(x)*L);
input_neighborhood2 = input_neighborhood1 - mean;
input_neighborhood = input_neighborhood2/sum_input_neighborhood;


%{
match_filt1 = -exp(-(x.*x)/(2*sigma*sigma)); 
%match_filt2 = max(match_filt1)-match_filt1; 
input_neighborhood1 = repmat(match_filt1,[L 1]); 
sum_input_neighborhood = sum(input_neighborhood1(:));
mean = sum_input_neighborhood/(length(x)*L);
input_neighborhood2 = input_neighborhood1 - mean;
input_neighborhood = input_neighborhood2;
%input_neighborhood = input_neighborhood2/sum_input_neighborhood;
%}
h{1} = zeros(15,16);
for i = 1:L
    for j = 1:length(x)
        h{1}(i+3,j+1) = input_neighborhood(i,j);
    end
end 

for k=1:11 
    ag = 15*k;
    h{k+1} = imrotate(h{1},ag,'bicubic','crop');
%    h{k+1} = wkeep(h{k+1},size(h{1}));
end 

for k=1:12
    R{k} = conv2(green_ch, h{k}, 'same');
end

rt = zeros(M,N); 
for i=1:M
    for j=1:N
        ER = [R{1}(i,j), R{2}(i,j), R{3}(i,j), R{4}(i,j), R{5}(i,j), R{6}(i,j),... 
                R{7}(i,j), R{8}(i,j), R{9}(i,j), R{10}(i,j), R{11}(i,j), R{12}(i,j)];
        rt(i,j) = max(ER);
    end
end
 
rmin = abs(min(rt(:)));
for m = 1:M
    for n = 1:N
        rt(m,n) = rt(m,n) + rmin;
    end
end
rmax = max(max(rt));
for m = 1:M
    for n = 1:N
        rt(m,n) = round(rt(m,n)*255/rmax);
    end
end

%rt = rt.*thresh1.*mask;