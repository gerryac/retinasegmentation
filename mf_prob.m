function prob = mf_prob(rgb,mask_s)

    sigma = 2.0;
    rt = mf(rgb(:,:,2),sigma);
    medval = median(median(rt));
    rt_prob = rt.*double(mask_s);
    for n = 1:size(rt,1)
        for m = 1:size(rt,2)
            if (rt_prob(n,m)<medval)
                rt_prob(n,m) = medval;
            end
        end
    end
    
    prob = (rt_prob-medval)/max(max(rt_prob));
end