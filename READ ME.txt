********************************************************************************************************

NUS ISS 2018
Bryan Tew Boon-Teck      Gerry Anggacipta       Ho Wei-Jing       Yong Lester Monar          Yong Jun-Jie
(A0163462H)		         (A0163261M)	        (A0163387W)	      (A0163415L)	             (A0163438B)

This code was worked on top of existing journal from Stanford students
for any queries please email gerry.anggacipta@u.nus.edu
********************************************************************************************************

------------------------
I. Running Hybrid J-Net
------------------------

In order to run segmentation using Hybrid J-Net, run the following script

hybrid_original.m



------------------------
II. Running J-Net
------------------------

J-Net component is located under directory 'Supervised/gan'. Source code for J-Net is in Python. 
In order to run J-Net alone,run the following scrypt using python 3:

inference.py

