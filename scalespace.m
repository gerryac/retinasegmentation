%
%   function [tp, fn, fp] = comp(gen,ref)
%
%       This code compares an image to a reference image
%
%       INPUT:
%       rgb        = original retinal image
%       mask       = mask (binary)
%       mask_s     = smaller mask (binary)
%
%       OUTPUTS:
%       kappa      = probability of vessel
%       gamma      = robust gradient
%

function [kappa,gamma] = scalespace(rgb,mask,mask_s)

% parameters for the scales we will use (we will increment linearly from
% smin to smax with a step "step")
smin = 1.5;
smax = 10;
step = .5;

[h w] = size(rgb(:,:,1));
ycbcr = rgb2ycbcr(rgb);

% we look at the luminance channel, which will be used for our algorithm
lum = double(ycbcr(:,:,1));
lum_norm = lum/max(lum(:));
lum_mask = lum_norm.*mask;

nb_s = (smax-smin)/step + 1;

% scale-space representation of the "derivatives" of the image
Ix = zeros(h,w,nb_s);
Iy = zeros(h,w,nb_s);
Ixx = zeros(h,w,nb_s);
Ixy = zeros(h,w,nb_s);
Iyy = zeros(h,w,nb_s);

for s = smin:step:smax
    % gaussian kernels and partial derivatives
    d=3*s;
    [X,Y]=meshgrid(-d:d,-d:d);
    k=1/(2*pi*s^2);
    gauss=k*exp(-0.5*(X.^2+Y.^2)/(s^2));
    gauss_x=(-X./(s^2)).*gauss;
    gauss_y=(-Y./(s^2)).*gauss;
    gauss_xx=gauss_x.*(-X./(s^2))+(-gauss/(s^2));
    gauss_yy=gauss_y.*(-Y./(s^2))+(-gauss/(s^2));
    gauss_xy=(-X./(s^2)).*gauss_y;
    
    % convolution using these gaussian kernels
    k = (s-smin)/step+1;
    Ix(:,:,k) = mask_s.*imfilter(lum_mask, s*gauss_x, 'conv');
    Iy(:,:,k) = mask_s.*imfilter(lum_mask, s*gauss_y, 'conv');
    Ixx(:,:,k) = mask_s.*imfilter(lum_mask, s^2*gauss_xx, 'conv');
    Iyy(:,:,k) = mask_s.*imfilter(lum_mask, s^2*gauss_yy, 'conv');
    Ixy(:,:,k) = mask_s.*imfilter(lum_mask, s^2*gauss_xy, 'conv');
end

% computation of the gradient and max curvature at each scale
grad = sqrt(Ix.^2+Iy.^2);
curv = zeros(h,w,nb_s);
for s = smin:step:smax
    k = (s-smin)/step+1;
    A = Ixx(:,:,k);
    B = Iyy(:,:,k);
    C = Ixy(:,:,k);
    lambdamax = (A+B + (4*C.^2 + (A-B).^2).^.5)/2;
    curv(:,:,k) = max(lambdamax,0);
    % corrective factor dependent on the scale
    curv(:,:,k) = curv(:,:,k)/s;
    grad(:,:,k) = grad(:,:,k)/s;
end


% we take the max on all the scales
gamma = max(grad,[],3)/max(grad(:));
kappa = max(curv,[],3)/max(curv(:));

end