clear all

nb_img = 20;

tp = zeros(nb_img,1);
tn = zeros(nb_img,1);
fp = zeros(nb_img,1);
fn = zeros(nb_img,1);

global supervised_method;
supervised_method = 'gan';
%load("nn2_results");
load(strcat("Supervised/",supervised_method,"/",supervised_method,"_results"));
nn2 = im_result;
load('multi-line_results');
multi = im_result;
diceCoeff = [];
for nImage = 1:nb_img
    
    str = strcat(['DRIVE/test/images/' num2str(nImage,'%02i') '_test.tif']);
    rgb = imread(str);
    
    hsv = rgb2hsv(rgb);
    
    % generation of the mask by thresholding
    val = hsv(:,:,3);
    thr_0 = min(graythresh(val), 0.3);
    mask = im2bw(val,thr_0);
    mask_s = imerode(mask, strel('disk',10));
    
    % application of the 5 different methods
    [scalespace_p,grad] = scalespace(rgb,mask,mask_s);
    morph_p = morph(rgb,mask,mask_s);
    %mf_p = mf_prob(rgb,mask_s);
    nn2_p = nn2{nImage}.*mask_s;
    multi_p = multi{nImage}.*mask_s;
    
    result = 1/4*(scalespace_p + morph_p + nn2_p + multi_p);
    
    %vss = imbinarize(result);
    vss = grow_tree_grad(result,grad,mask_s);
    
    % we save the image in an "output_original_hybrid" directory
    str = strcat(['output_original_hybrid/' num2str(nImage,'%02i') '_segmented.png']);
    imwrite(vss,str,'png');
    
    % visualisation of the performance
    mstr = strcat(['DRIVE/test/1st_manual/' num2str(nImage,'%02i') '_manual1.gif']);
    manual = im2double(imread(mstr));
    
    [tp(nImage), tn(nImage), fp(nImage), fn(nImage)] = comp(vss,manual,mask_s);
    
    diceCoeff = [diceCoeff dice(imbinarize(imread(mstr)),vss)];
    
end

accuracy = (tp+tn)./(fp+fn+tp+tn);
precision = tp./(tp+fp);
sensitivity = tp./(tp+fn);
specificity = tn./(tn+fp);
f1score = 2.*(precision.*sensitivity)./(precision+sensitivity);

performance_table = [accuracy sensitivity precision specificity diceCoeff' f1score];

disp(['Avg accuracy = ', num2str(mean(accuracy))]);
disp(['Avg precision = ', num2str(mean(precision))]);
disp(['Avg sensitivity = ', num2str(mean(sensitivity))]);
disp(['Avg specificity = ', num2str(mean(specificity))]);
disp(['Avg dice coefficient = ', num2str(mean(diceCoeff))]);
disp(['Avg f1score = ', num2str(mean(f1score))]);